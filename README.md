# TUDU Drag & Drop Planner
<img src="https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/05-tudu/-/raw/master/tudu/resources/readme/tudu_hero_image_m.png"
alt="TUDU drag&drop planner" width="1000"/>

## :memo: Kratak opis
TUDU je nedeljni drag & drop planer, sa integrisanom listom obaveza (todo listom). Uz pomoć ove aplikacije možete organizovati svoje planove na pregledan  i sitematizovan način.

U listu obaveza se dodaju sve obaveze koje planirate da obavite u narednom periodu i zatim im se dodeljuje prioritet, tako da imate sistematizovan spisak obaveza koji omogućava da nikad ne zaborative na bitan sastanak, porodični ručak ili neku drugu obavezu.

Iz liste obaveze možete premestiti u nedeljni kalendar i tako na pregledan način u svakom trenutku videti šta vas očekuje ove, ili bilo koje naredne nedelje.

## :wrench: Postupak instalacije i pokretanja:
* Preuzeti i instalirati Qt Creator - [Online Installer](https://www.qt.io/download-qt-installer) ili [Source Code](https://www.qt.io/download-open-source)
* Klonirati direktorijum na računar
``` git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/05-tudu.git ```
* Otvoriti tudu.pro iz kloniranog direktorijuma u programu Qt Creator
* Pokrenuti program u razvojnom okruženju
``` Ctrl + R ```

## :computer: Developers

- [Ikovic Svetozar, 69/2016](https://gitlab.com/toswe)
- [Mihajlo Živković, 87/2016](https://gitlab.com/mi16087)
- [Lazar Ristic, 150/2016](https://gitlab.com/lristic)
- [Dimitrije Petrović, 466/2017](https://gitlab.com/mitokv)
- [Dimitrije Stamenić, 260/2016](https://gitlab.com/stamd)
